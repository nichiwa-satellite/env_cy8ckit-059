cd %~dp0

@powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin

cinst -y packages.config

cpack cy8ckit-059_kit\cy8ckit-059_kit.nuspec
cinst cy8ckit-059_kit -y -s "http://chocolatey.org/api/v2/;%cd%"
