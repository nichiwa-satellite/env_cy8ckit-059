# CY8CKIT-059 開発環境セットアップ用プロジェクト

[CY8CKIT-059](http://japan.cypress.com/documentation/development-kitsboards/cy8ckit-059-psoc-5lp-prototyping-kit-onboard-programmer-and)の開発環境のセットアップを行います。

## セットアップ手順

1. 次のどちらかの方法でこのリポジトリの内容を取得し、
    * [左にあるダウンロードアイコン](https://bitbucket.org/nichiwa-satellite/env_cy8ckit-059/downloads)からこのリポジトリをダウンロードして解凍
    * gitクライアントを使用して[このリポジトリ](https://bitbucket.org/nichiwa-satellite/env_cy8ckit-059.git)をチェックアウト
0. install.batを右クリックして「管理者として実行」する。

## install.batでインストールするもの

chocolatey
: windows上の環境設定を自動化するためのプログラム。  
  プログラムのリストを定義しておくと、インストールされていないものだけインストールする。

SourceTree
: Gitクライアントの一つ。  
  BitBucketをはじめとしたさまざまなGitホスティングサービスに対応している。

GitHub Desktop
: Gitクライアントの一つ。  
  BitBucketとの連携は無く機能も限定されているが、Github Flowで進める分にはとても扱いやすい

Atom
: GitHub社の出しているエディタ。  
  Markdown記法のプレビューができる。

CY8CKIT-059 Kit Setup
: CYPRESS社提供の開発キット。「Kit Design Files, Creator, Programmer, Documentation, Examples」の全てがインストールされる。